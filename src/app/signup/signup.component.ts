import { Component, OnInit, NgZone, Input } from '@angular/core';
import * as firebase from 'firebase';
import { SunspotdataService } from 'src/app/sunspotdata.service';
import { FormGroup, FormBuilder, FormArray, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
//import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  public signupForm: FormGroup;
  public submitted: boolean = false;
  public loading: boolean = false;

  constructor(private formBuilder: FormBuilder,
    public translate: TranslateService,
    public service: SunspotdataService,
    public router: Router,
   // private toastr: ToastrService,
    public zone: NgZone) {
    if (this.service.isLoggedIn) {
      router.navigate(['']);
    }
  }

 
  ngOnInit() {
    var self = this;

    var vid:any = document.getElementById('video');
    if (vid) {
      vid.muted = true;
    }

    self.signupForm = self.formBuilder.group({
      firstName: ['', [Validators.required, Validators.minLength(3)]],
      lastName: ['', [Validators.required, Validators.minLength(3)]],
      email: ['', Validators.compose([
        Validators.required,
        Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$'),
      ])],
      phoneNo: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(16)]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      c_password: ['', [Validators.required, Validators.minLength(6)]],
    });
   
  }

  get f() { return this.signupForm.controls; }

  checkPassword() {
    if (this.signupForm.get("password").value == this.signupForm.get("c_password").value) {
      return true;
    } else {
      return false;
    }
  }

  submitSignupForm() {
    var self = this;
    self.submitted = true;
    if (this.signupForm.valid && self.signupForm.get("password").value == self.signupForm.get("c_password").value) {
      self.submitted = false;
      self.loading = true;
      firebase.auth().createUserWithEmailAndPassword(self.signupForm.get("email").value, self.signupForm.get("password").value)
        .then((user) => {
          if (firebase.auth().currentUser) {
            self.saveUserData(firebase.auth().currentUser.uid);
          } else {
            self.loading = false;
          //  self.toastr.error('Account not created, kindly try again', '', { timeOut: 4000 })
          }
        }).catch((error) => {
          self.loading = false;
         // self.toastr.error(error.message, '', { timeOut: 3000 })
        });
    }
  }

  saveUserData(uid: any) {
  
    var self = this;
    var formData = {
      firstName: self.signupForm.get("firstName").value,
      lastName: self.signupForm.get("lastName").value,
      email: self.signupForm.get("email").value,
      phoneNo: self.signupForm.get("phoneNo").value,
      uid: uid,
      active: true,
      timeStamp: Number(new Date())
    };

    firebase.database().ref('users/' + uid).update(formData).then((result) => {
    
     //self.toastr.success('You have signedup successfully.', '', { timeOut: 4000 })
      self.signupForm.reset();

      self.service.setUser(formData) ;
      self.loading = false;
      self.router.navigate(['']);
    }).catch((err) => {
      self.loading = false;
      //self.toastr.error(err, '', { timeOut: 4000 })
      
    });
  }


}
