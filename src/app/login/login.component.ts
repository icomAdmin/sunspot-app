import { Component, OnInit, NgZone, Input } from '@angular/core';
import * as firebase from 'firebase';
import { SunspotdataService } from 'src/app/sunspotdata.service';
import { FormGroup, FormBuilder, FormArray, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public loading: boolean = false;
  public uid: any;
  public user: any = {
    email: '',
    password: ''
  }
  constructor(public translate: TranslateService,
    private formBuilder: FormBuilder,
    public service: SunspotdataService,
    public router: Router,
    private toastr: ToastrService,
    public zone: NgZone) {
    if (this.service.isLoggedIn) {
      router.navigate(['']);
    }
  }

  ngOnInit(): void {
   var vid:any = document.getElementById('video');
    if (vid) {
      vid.muted = true;
    }
  }

  loginAccount() {
    var self = this;
    self.loading = true;
    firebase.auth().signInWithEmailAndPassword(this.user.email, this.user.password)
      .then((user) => {
        if (user) {
          self.uid = user.user.uid;
          self.getUserData();
        }
        else {
         self.toastr.error('user not exist','', { timeOut: 4000 } )
         self.loading = false;
        }
      })
      .catch((e) => {
        self.loading = false;
        self.toastr.error(e.message,'', { timeOut: 4000 } )
       
      });
  }

  getUserData() {
    var self = this;
    firebase.database().ref().child('users/' + self.uid)
      .once('value', (snapshot) => {
        self.zone.run(() => {
          var user = snapshot.val();
          self.loading = false;
          if (user && user.active) {  
            self.service.setUser(user) ;
            self.toastr.success('You have logged in successfully','', { timeOut: 4000 } )
            self.router.navigate(['']);
            
          } else {
            self.toastr.error('You have been blocked by Admin','', { timeOut: 4000 } )
          }
        });
      }).catch((e) => {
        self.loading = false;
       self.toastr.error(e.message,'', { timeOut: 4000 } );
      
      })
  }

}
