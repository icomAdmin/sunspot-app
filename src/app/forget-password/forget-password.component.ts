import { Component, OnInit, NgZone, Input } from '@angular/core';
import * as firebase from 'firebase';
import { SunspotdataService } from 'src/app/sunspotdata.service';
import { FormGroup, FormBuilder, FormArray, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.scss']
})
export class ForgetPasswordComponent implements OnInit {

  public passwordForm: FormGroup;
  public submitted: boolean = false;
  public loading: boolean = false;

  constructor(private formBuilder: FormBuilder,
    public translate: TranslateService,
    public service: SunspotdataService,
    public router: Router,
    public zone: NgZone) {
    if (this.service.isLoggedIn) {
      router.navigate(['']);
    }
  }

  ngOnInit() {
    var self = this;
    var vid:any = document.getElementById('video');
    if (vid) {
      vid.muted = true;
    }
    self.passwordForm = self.formBuilder.group({
      email: ['', Validators.compose([
        Validators.required,
        Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$'),
      ])],
    });
   
  }

  get f() { return this.passwordForm.controls; }
  
  
  resetPassword() {
   
    var self = this;
    this.submitted = true ;
    if(this.passwordForm.valid) {
      this.submitted = false ;
      self.loading = true ;
      firebase.auth().sendPasswordResetEmail(self.passwordForm.get("email").value)
      .then(() => {
        self.loading = false ;
        self.router.navigate(['/login']);
      })
      .catch((e) => {
        self.loading = false ;
      })
    }
    
    }

}
