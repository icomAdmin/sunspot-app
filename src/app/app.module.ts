import { BrowserModule } from '@angular/platform-browser';

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { MyProfileComponent } from './my-profile/my-profile.component';
import { HeaderComponent } from './header/header.component';
import { PlaceDetailComponent } from './place-detail/place-detail.component';
import { SignupComponent } from './signup/signup.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PinchZoomModule } from 'ngx-pinch-zoom';

import * as firebase from 'firebase';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { ToastrModule, ToastNoAnimation, ToastNoAnimationModule } from 'ngx-toastr';
import { NgxLoadingModule } from 'ngx-loading';
import { AgmCoreModule } from '@agm/core';
import { NgxGeoautocompleteModule } from 'ngx-geoautocomplete';
import { FooterComponent } from './footer/footer.component';

import { OwlDateTimeModule, OwlNativeDateTimeModule, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderTwoComponent } from './header-two/header-two.component';
import { HomeTwoComponent } from './home-two/home-two.component';
import { FooterTwoComponent } from './footer-two/footer-two.component';
import { PaymentsComponent } from './payments/payments.component';

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';

const appRoutes: Routes = [
  // { path: 'home', component: HomeComponent },
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'my-profile', component: MyProfileComponent },
  { path: 'place-detail/:id', component: PlaceDetailComponent },
  { path: 'forget-password', component: ForgetPasswordComponent },
  { path: 'home', component: ForgetPasswordComponent },
  { path: 'seat-payment', component: PaymentsComponent },
  { path: 'home-two', component: HomeTwoComponent },
  { path: '', redirectTo: '/home-two', pathMatch: 'full' },
];

/*var firebaseConfig = {
  apiKey: "AIzaSyCjD1v7PKXPnGNyDTd9f2KIe77c1UVuMzs",
  authDomain: "sunspot-8ffc2.firebaseapp.com",
  databaseURL: "https://sunspot-8ffc2.firebaseio.com",
  projectId: "sunspot-8ffc2",
  storageBucket: "sunspot-8ffc2.appspot.com",
  // messagingSenderId: "23528961491",
  appId: "1:23528961491:web:5ea386f05e9423fd156b26",
  measurementId: "G-KK71358HRK"
};*/
  var firebaseConfig = {
      apiKey: "AIzaSyCjD1v7PKXPnGNyDTd9f2KIe77c1UVuMzs",
      authDomain: "sunspot-8ffc2.firebaseapp.com",
      databaseURL: "https://sunspot-8ffc2.firebaseio.com",
      projectId: "sunspot-8ffc2",
      storageBucket: "sunspot-8ffc2.appspot.com",
      messagingSenderId: "23528961491",
      appId: "1:23528961491:web:5ea386f05e9423fd156b26",
      measurementId: "G-KK71358HRK"
    };
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    MyProfileComponent,
    HeaderComponent,
    PlaceDetailComponent,
    SignupComponent,
    ForgetPasswordComponent,
    FooterComponent,
    HeaderTwoComponent,
    HomeTwoComponent,
    FooterTwoComponent,
    PaymentsComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    PinchZoomModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDGmkGmJzanvRjTGbgjMIoiXGSTTnHnJNE'
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    HttpClientModule,
    NgxGeoautocompleteModule.forRoot(),
    NgxLoadingModule.forRoot({
      fullScreenBackdrop: true, backdropBorderRadius: '3px'
    }),
    ToastNoAnimationModule.forRoot(),
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
  ],
  providers: [
    // use french locale
    { provide: OWL_DATE_TIME_LOCALE, useValue: 'fr' },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}