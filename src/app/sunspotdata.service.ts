import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import * as firebase from 'firebase';
import * as moment from 'moment';
import { Component, NgZone } from '@angular/core';
import { Subject } from 'rxjs-compat';
declare var google: any;

@Injectable({
  providedIn: 'root'
})
export class SunspotdataService {

  public locationOBS: Subject<string> = new Subject<string>();

  public isLoggedIn: boolean = false;
  public beachesList: any = [];
  public fullBeachesList: any = [];
  public userBookingsList: any = [];
  public selectedDateBookingsList: any = [];
  public activeUserBookings: any = [];
  public seatMapList: any = [];
  public activeBeach: any = {};
  public activeBeachId: any;

  public seatHeight:string = "150px";
  public userLocation: any = {};
  public userData: any;
  public colorArray: any = ['#FBD241', '#FE6329', '#D34CB1', '#A635FF', '#3570FF', '#33F8EF', '#199953', '#663A00', '#404040']

  public days: any = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
  public selectedDate: any;
  public homeDate: any;
  public todaysdate: any;

  public totalPrice: number = 0;
  public bookedSeats: any = [];

  constructor(public router: Router,
    public zone: NgZone) {

    this.todaysdate = new Date().getFullYear() + '-' + ('0' + (new Date().getMonth() + 1)).slice(-2) + '-' + ('0' + new Date().getDate()).slice(-2);
    this.selectedDate = this.todaysdate;
    this.homeDate = this.todaysdate;
    this.getUserLocation();
    if (localStorage.getItem('sunspotUser')) {
      this.userData = JSON.parse(localStorage.getItem('sunspotUser'));
      this.isLoggedIn = true;
    } if (!this.seatMapList.length) {
      this.getSeatMapsData();
    } if (!this.userBookingsList.length) {
      this.getUserBookingsData();
    } if (!this.beachesList.length) {
      this.getBeachesData();
    }
  }

  setUser(user) {
    this.userData = user;
    this.isLoggedIn = true;
    localStorage.setItem('sunspotUser', JSON.stringify(user));
  }


  getSeatMapsData() {
    var self = this;

    self.zone.run(() => {
      firebase.database().ref().child('seatMaps')
        .once('value', (snapshot) => {
          var data = snapshot.val();
          self.seatMapList = [];
          for (var key in data) {
            data[key].seatId = key;
            self.seatMapList.push(data[key]);
          }
          self.seatMapList.reverse();
        }).catch((err) => {
          alert(err);
        });
    });

  }

  getSeatColor(color) {
    if (!color) {
      return 0;
    } else {
      var colorIndex = this.colorArray.indexOf(color);
      if (colorIndex >= 0) {
        return colorIndex + 1;
      }
    }
    return 0;
  }

  getUserBookingsData() {
    var self = this;

    self.zone.run(() => {
      firebase.database().ref().child('userBookings')
        .once('value', (snapshot) => {
          var data = snapshot.val();
          self.userBookingsList = [];
          for (var key in data) {
            data[key].bookingId = key;
            self.userBookingsList.push(data[key]);
          }
          self.newBookingDataListner();
        }).catch((err) => {
          alert(err);
        });
    });

  }

  newBookingDataListner() {
    var self = this;
    var bookingListner;
    bookingListner = firebase.database().ref().child('userBookings');
    bookingListner.on("child_added", function (snapshot, previousChildKey) {

      self.zone.run(() => {
        var data = snapshot.val();
        data.bookingId = snapshot.key;
        if (self.userBookingsList.findIndex(ob => ob.bookingId == data.bookingId) >= 0) {
          return;
        }
        self.userBookingsList.push(data);
        console.log(self.userBookingsList);
      });
    });
  }

  getBeachesData() {
    var self = this;

    self.zone.run(() => {
      firebase.database().ref().child('beaches')
        .once('value', (snapshot) => {
          var data = snapshot.val();
          self.beachesList = [];
          for (var key in data) {
            if(data[key].active) {
              data[key].beachId = key;
              self.beachesList.push(data[key]);
            }
          }
          if (self.beachesList.length) {
            self.getSpecificDateData();
            if (self.activeBeachId) {
              self.getActiveBeachData();
            }
            self.getBeachDistance();
            self.getActiveUserBookings();
          }
        }).catch((err) => {
          alert(err);
        });
    });

  }

  getActiveUserBookings() {
    this.activeUserBookings = [];
    if (this.isLoggedIn && this.beachesList.length && this.userBookingsList.length) {
      this.userBookingsList.sort((a, b) => (a.bookingDate >= b.bookingDate) ? 1 : -1);

      for (var i = 0; i < this.userBookingsList.length; i++) {
        if (this.userBookingsList[i].uid == this.userData.uid) {
          var bookingData: any = Object.assign({}, this.userBookingsList[i]);
          var beachData = Object.assign({}, this.beachesList.find(ob => ob['beachId'] == bookingData.beachId));
          if (beachData) {
            bookingData.beach = beachData;
          } if (!this.userData.nextBooking && this.userData.nextBooking != 0 && Number(new Date(bookingData.bookingDate)) >= Number(new Date(this.todaysdate))) {
            this.userData.nextBooking = i;
          }
          this.activeUserBookings.push(bookingData);
        }
      }
      if (this.activeUserBookings.length) {
        this.activeUserBookings.sort((a, b) => (Number(new Date(a.bookingDate))  < Number(new Date(b.bookingDate))) ? 1 : -1);
      }
      this.userData.nextBooking = this.activeUserBookings.length - this.userData.nextBooking - 1;
    }

  }

  getActiveBeachData() {
    var self = this;
    this.activeBeach = this.beachesList.find(ob => ob['beachId'] == this.activeBeachId);

    if (this.bookedSeats.length)
      for (var i = 0; i < this.bookedSeats.length; i++) {
        this.activeBeach.seatMaps.rows[this.bookedSeats[i].row].col[this.bookedSeats[i].col].booked = false;
      }
    this.bookedSeats = [];
    this.totalPrice = 0;

    if (self.activeBeachId) {
      self.selectedDateBookingsList = [];

      var dt =  moment(new Date( self.selectedDate)).format('YYYY-MM-DD') 
      for (var i = 0; i < self.userBookingsList.length; i++) {
        if ( moment(new Date(self.userBookingsList[i].bookingDate )).format('YYYY-MM-DD') == dt && self.userBookingsList[i].beachId == self.activeBeachId) {
          for (var j = 0; j < self.userBookingsList[i].bookedSeats.length; j++) {
            self.userBookingsList[i].bookedSeats[j]['uid'] = self.userBookingsList[i].uid;
            self.selectedDateBookingsList.push(self.userBookingsList[i].bookedSeats[j]);
          }
        }
      }
    }
   
    if (self.selectedDateBookingsList.length && self.activeBeach.seatMaps) {
      for (var i = 0; i < self.activeBeach.seatMaps.rows.length; i++) {
        for (var j = 0; j < self.activeBeach.seatMaps.rows[i].col.length; j++) {
          var index = this.selectedDateBookingsList.findIndex(x => x.row === i && x.col === j);
          if (index >= 0) {
            self.activeBeach.seatMaps.rows[i].col[j].reserved = true;
          }
        }
      }
    }
  
    if(self.activeBeach && self.activeBeach.seatMaps && self.activeBeach.seatMaps.rows &&
      self.activeBeach.seatMaps.rows.length ) {
        self.checkSeatHeight(self.activeBeach.seatMaps.rows.length)
    }
  }

  getUserLocation() {
    var self = this;
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        self.userLocation.lat = position.coords.latitude;
        self.userLocation.lng = position.coords.longitude;
        self.userLocation.zoom = 16;
        self.getLocationFormattedAddress()
        if (self.beachesList.length) {
          self.getBeachDistance();
        }
      });
    }
  }

  getLocationFormattedAddress() {
    var self = this;
    let geocoder = new google.maps.Geocoder;
    let latlng = { lat: self.userLocation.lat, lng: self.userLocation.lng };
    geocoder.geocode({ 'location': latlng }, (results) => {
      if (results[0]) {
        self.userLocation.location = results[0].formatted_address;
        var tempArray = results[0].address_components;
        for (var i = 0; i < tempArray.length; i++) {
          var typesArray = tempArray[i].types;
          for (var j = 0; j < typesArray.length + 1; j++) {
            if (typesArray[j] == "locality") {
              self.userLocation.city = tempArray[i].long_name;
              self.locationOBS.next();
              break;
            }
          }
        }
      }
    });

  }

  getBeachDistance() {
    var self = this;
    for (var i = 0; i < self.beachesList.length; i++) {
      var distance = self.inNearbyRange(self.userLocation.lat, self.userLocation.lng, self.beachesList[i].latitude, self.beachesList[i].longitude)
      self.beachesList[i].distance = distance;
    }
    self.beachesList.sort((a, b) => (a.distance >= b.distance) ? 1 : -1);
    self.fullBeachesList = self.beachesList;
  }

  inNearbyRange(userLat, userLng, beachLat, beachLng): number {
    var R = 6371;
    var dLat = (beachLat - userLat) * (Math.PI / 180)
    var dLon = (beachLng - userLng) * (Math.PI / 180);
    var a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos((userLat) * (Math.PI / 180)) * Math.cos((beachLat) * (Math.PI / 180)) *
      Math.sin(dLon / 2) * Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return Math.round(d * 10) / 10;
  }

  getSpecificDateData() {
    var self = this;
    this.bookedSeats = [];
    this.totalPrice = 0;
    var dt = moment(self.selectedDate).format("YYYY-MM-DD")
    for (var i = 0; i < self.beachesList.length; i++) {
      self.beachesList[i].seatMaps = '';
      self.homeDate = self.selectedDate;
      var index = self.seatMapList.findIndex(x => x.beachId === self.beachesList[i].beachId);
     
      if (index >= 0) {
        var seatData = self.seatMapList[index];
        if (seatData.specificDates && seatData.specificDates.length) {
          
          var index2 = seatData.specificDates.findIndex(x => x.date === dt );
          if (index2 >= 0) {
            self.beachesList[i].seatMaps = seatData.specificDates[index2];
          }
        }
        if (!self.beachesList[i].seatMaps) {
          var weekDayName = ''
          var date = new Date(self.selectedDate);
          var day = date.getDay();
          weekDayName = self.days[day];
          if (seatData.weekDays && seatData.weekDays[weekDayName]) {
            self.beachesList[i].seatMaps = seatData.weekDays[weekDayName];
          }
        }
      }
    }
    if (self.activeBeachId) {
      self.getActiveBeachData();
    }
  }

  checkFavouriteBeach(beachId) {
    if (this.userData && this.userData.favouriteBeaches && this.userData.favouriteBeaches.length &&
      this.userData.favouriteBeaches.findIndex(x => x.beachId === beachId) >= 0) {
      return true;
    } else {
      return false;
    }
  }

  updateFavouriteBeach(beachKey) {
    var self = this;
    if (self.userData && self.userData.favouriteBeaches) {
      var index = self.userData.favouriteBeaches.findIndex(x => x.beachId === beachKey);
      if (index >= 0) {
        self.userData.favouriteBeaches.splice(index, 1);
      } else {
        self.userData.favouriteBeaches.push({ beachId: beachKey })
      }
    } else {
      self.userData.favouriteBeaches = [];
      self.userData.favouriteBeaches.push({ beachId: beachKey });
    }

    firebase.database().ref().child('users/' + self.userData.uid).update(self.userData).then((result) => {
      self.setUser(self.userData);
    }).catch((error) => {
      var index = self.userData.favouriteBeaches.findIndex(x => x.beachId === beachKey);
      if (index >= 0) {
        self.userData.favouriteBeaches.splice(index, 1);
      }
    })
  }

  changeTimeFormat(oldTime) {
    if (!oldTime) {
      return '';
    }
    var timeArray = oldTime.split(":");
    var HH = parseInt(timeArray[0]);
    var min = timeArray[1];

    var AMPM = HH >= 12 ? "PM" : "AM";

    if (HH == 0) {
      HH = 12;
    } else if (HH > 12) {
      HH = HH - 12;
    }

    return HH + ":" + min + " " + AMPM;
  }

  logoutUser() {
    this.userData = {};
    this.isLoggedIn = false;
    localStorage.removeItem('sunspotUser');
    this.router.navigate(['/login']);
  }


  checkSeatHeight(length) {

    var height = 370/length ;
    if(height>=150){
      this.seatHeight = '150px';
    } else if(height<15) {
      this.seatHeight = '15px';
    }else {
      this.seatHeight = height + 'px';
    }
  }

}
