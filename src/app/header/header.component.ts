import { Component, OnInit, NgZone, Input } from '@angular/core';
import { SunspotdataService } from 'src/app/sunspotdata.service';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { UtilsService } from '../utils.service';
import { TranslateService } from '@ngx-translate/core';
//import flatpickr from "flatpickr";
import { DateTimeAdapter } from 'ng-pick-datetime';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public showLocation:boolean = false;
  public userSettings1: any = {
    noOfRecentSearchSave: 3,
    inputPlaceholderText: 'Enter/Entrez  location/localisation',
    inputString: this.service.userLocation.city,
  };

  public tempLocation:any = {};
  public days: any = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']

  constructor(public service: SunspotdataService,
    public translate: TranslateService,
    public utils: UtilsService,
    public dateAdapter: DateTimeAdapter<any>,
    public router: Router) { 
   
  }

  ngOnInit(): void {
    this.updateLanguage();
  }

  autoCompleteCallback1(data: any): any {

    this.tempLocation.lat = data.data.geometry.location.lat;
    this.tempLocation.lng = data.data.geometry.location.lng;
    var tempArray = data.data.address_components;
    for (var i = 0; i < tempArray.length; i++) {
      var typesArray = tempArray[i].types;
      for (var j = 0; j < typesArray.length + 1; j++) {
        if (typesArray[j] == "locality") {
          this.tempLocation.city = tempArray[i].long_name ;
        }
      }
    }
    this.updateLocation()
   
  }

  updateLocation() {

    this.service.userLocation.city = this.tempLocation.city ;
    this.service.userLocation.lat = this.tempLocation.lat ;
    this.service.userLocation.lng = this.tempLocation.lng ;
    this.service.getBeachDistance();

    this.userSettings1 = {
      noOfRecentSearchSave: 3,
      inputPlaceholderText: 'Enter location',
      inputString: this.service.userLocation.city,
    };

    var btn:any =  document.getElementById('closeLocationMd');
    if(btn) {
      btn.click();
    }
  }

  updateLanguage() {
    this.dateAdapter.setLocale(this.utils.language);
  }
}
