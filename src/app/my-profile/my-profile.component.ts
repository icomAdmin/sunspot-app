import { Component, OnInit, NgZone } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as firebase from 'firebase';
import { Router } from '@angular/router';
import { SunspotdataService } from 'src/app/sunspotdata.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';

//import { CreditCardValidator } from 'ngx-credit-cards';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.scss']
})
export class MyProfileComponent implements OnInit {

  public qrView: any = false;
  public submitted = false;
  public loading: boolean = false;
  public profileForm: FormGroup;

  public selectedFile: any;
  public imgURL: any;
  constructor(public router: Router,
    public translate: TranslateService,
    public zone: NgZone,
    public toastr: ToastrService,
    public formBuilder: FormBuilder,
    public service: SunspotdataService) {
    if (!this.service.isLoggedIn) {
      router.navigate(['']);
    }
  }

  ngOnInit() {
    var self = this;
    this.profileForm = this.formBuilder.group({
      firstName: ['', [Validators.required, Validators.minLength(3)]],
      lastName: ['', [Validators.required, Validators.minLength(3)]],
      email: ['', Validators.compose([
        Validators.required,
        Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$'),
      ])],
      phoneNo: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(16)]],
      // accountTitle: ['', [Validators.required, Validators.minLength(3)]],
      // accountNumber: ['', [Validators.required, Validators.minLength(16), Validators.maxLength(16)]],
      password: ['', [Validators.minLength(6)]],
      new_password: ['', [Validators.minLength(6)]],
    });

    if (this.service.userData) {
      this.fillFormData();
    }

  }

  onWindowScroll(event) {
    var scrollPosition = event.srcElement.scrollTop;
  }

  scrollUp() {
    var div = document.getElementById('resrvationListDiv');
    div.scrollTop = 0;
  }

  fillFormData() {
    var self = this;
    var data: any = self.service.userData;

    if (data) {
      self.profileForm.get("firstName").setValue(data.firstName);
      self.profileForm.get("lastName").setValue(data.lastName);

      self.profileForm.get("email").setValue(data.email);

    } if (data.phoneNo) {
      self.profileForm.get("phoneNo").setValue(data.phoneNo);
    }
    if (data.imageUrl) {
      self.imgURL = data.imageUrl;
    }

  }

  onChangeFile(event: EventTarget) {
    let eventObj: MSInputMethodContext = <MSInputMethodContext>event;
    let target: HTMLInputElement = <HTMLInputElement>eventObj.target;
    this.selectedFile = target.files[0];
    const file = target.files[0];
    const reader = new FileReader();
    reader.onload = e => this.imgURL = reader.result;
    reader.readAsDataURL(file);
  }

  checkPassword() {
    if ((this.profileForm.get("new_password").value && this.profileForm.get("password").value) ||
      (!this.profileForm.get("new_password").value && !this.profileForm.get("password").value)) {
      return true;
    } else {
      return false;
    }
  }

  setVisibility() {
    if (this.qrView == false) {
      this.qrView = true;
    } else {
      this.qrView = false;
    }
  }

  get f() { return this.profileForm.controls; }

  updateProfile() {
    var self = this;
    self.submitted = true;
    if (self.profileForm.valid && this.checkPassword()) {
      self.loading = true;
      if (this.selectedFile) {
        this.uploadPhoto();
      } else if (self.profileForm.get("new_password").value) {
        this.changePassword('');
      } else {
        this.saveProfile('');
      }
    }
  }

  uploadPhoto() {
    var self = this;
    const randomId = Math.random().toString(36).substring(2);
    firebase.storage().ref(randomId).put(this.selectedFile).then((result) => {
      result.ref.getDownloadURL().then((url) => {
        if (self.profileForm.get("new_password").value) {
          self.changePassword(url);
        } else {
          self.saveProfile(url);
        }
      });
    }).catch((err) => {
      self.loading = false;
      self.toastr.error(err.message, '', { timeOut: 4000 });

    });
  }

  changePassword(url) {
    var self = this;

    firebase.auth().signInWithEmailAndPassword(self.service.userData.email, self.profileForm.get("password").value)
      .then((user) => {
        if (user) {
          firebase.auth().currentUser.updatePassword(self.profileForm.get("new_password").value).then(() => {
            self.saveProfile(url);
          }).catch((error) => {
            self.loading = false;
            self.toastr.error(error.message, '', { timeOut: 4000 });
          });
        } else {
          self.loading = false;
          self.toastr.error('current password is not correct', '', { timeOut: 4000 });
        }
      })
      .catch((error) => {
        self.loading = false;
        self.toastr.error(error.message, '', { timeOut: 4000 });
      });
  }

  saveProfile(url) {
    var self = this;
    var formData = {
      firstName: self.profileForm.get("firstName").value,
      lastName: self.profileForm.get("lastName").value,
      phoneNo: self.profileForm.get("phoneNo").value,
    };

    if (url) {
      formData['imageUrl'] = url;
    }

    firebase.database().ref().child('users/' + self.service.userData.uid).update(formData)
      .then((result) => {
        self.service.userData.firstName = formData.firstName;
        self.service.userData.lastName = formData.lastName;
        self.service.userData.phoneNo = formData.phoneNo;
       // self.service.userData.accountTitle = formData.accountTitle;
       // self.service.userData.accountNumber = formData.accountNumber;
        if (url) {
          self.service.userData.imageUrl = url;
          self.imgURL = url;
        }
        self.service.setUser(self.service.userData);


        self.profileForm.get("password").setValue('');
        self.profileForm.get("new_password").setValue('');
        self.loading = false;
        self.toastr.success("Profile data updated successfully.", '', { timeOut: 4000 });
      }).catch((error) => {
        self.loading = false;
        self.toastr.error(error.message, '', { timeOut: 4000 });

      })
  }

}

