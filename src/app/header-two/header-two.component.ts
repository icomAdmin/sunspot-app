import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SunspotdataService } from '../sunspotdata.service';

@Component({
  selector: 'app-header-two',
  templateUrl: './header-two.component.html',
  styleUrls: ['./header-two.component.scss']
})
export class HeaderTwoComponent implements OnInit {

  public showAutoComplete: boolean = false;

  public showLocation:boolean = false;
  public userSettings1: any = {
    noOfRecentSearchSave: 3,
    inputPlaceholderText: 'Enter location',
    inputString: this.service.userLocation.city,
  };

  public tempLocation:any = {};
  public days: any = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']

  constructor(public service: SunspotdataService,
    public router: Router) { 
   
  }

  ngOnInit(): void {
    var url = this.router.routerState.snapshot.url
    if(url=='/home'){
      this.showAutoComplete = true;
    }
    else{
      this.showAutoComplete = false;
    }
    this.service.locationOBS.subscribe((value) => {
      this.userSettings1 = {
        noOfRecentSearchSave: 3,
        inputPlaceholderText: 'service.self.userLocation.city',
        inputString: this.service.userLocation.city,
      };
    });
  }

  autoCompleteCallback1(data: any): any {

    this.tempLocation.lat = data.data.geometry.location.lat;
    this.tempLocation.lng = data.data.geometry.location.lng;
    var tempArray = data.data.address_components;
    for (var i = 0; i < tempArray.length; i++) {
      var typesArray = tempArray[i].types;
      for (var j = 0; j < typesArray.length + 1; j++) {
        if (typesArray[j] == "locality") {
          this.tempLocation.city = tempArray[i].long_name ;
        }
      }
    }
    this.updateLocation()
   
  }

  updateLocation() {

    this.service.userLocation.city = this.tempLocation.city ;
    this.service.userLocation.lat = this.tempLocation.lat ;
    this.service.userLocation.lng = this.tempLocation.lng ;
    this.service.getBeachDistance();

    this.userSettings1 = {
      noOfRecentSearchSave: 3,
      inputPlaceholderText: 'Enter location',
      inputString: this.service.userLocation.city,
    };

    var btn:any =  document.getElementById('closeLocationMd');
    if(btn) {
      btn.click();
    }
  }
}
