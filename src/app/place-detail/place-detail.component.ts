import { Component, OnInit, NgZone } from '@angular/core';
import * as firebase from 'firebase';
import { SunspotdataService } from 'src/app/sunspotdata.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-place-detail',
  templateUrl: './place-detail.component.html',
  styleUrls: ['./place-detail.component.scss']
})
export class PlaceDetailComponent implements OnInit {

  public settingsData: any = {};
  public submitted: boolean = false;
  public fullHeight:string;
  public seatFullHeight:string;

  public loading: boolean = false;
  public showSeats: any = false;
  public chairImagesList: any = ['/assets/imgs/1-White.svg', '/assets/imgs/2-Yellow.svg', '/assets/imgs/3-Orange.svg',
    '/assets/imgs/4-Pink.svg', '/assets/imgs/5-Purple.svg', '/assets/imgs/6-Blue.svg', '/assets/imgs/7-Cyan.svg',
    '/assets/imgs/8-Green.svg', '/assets/imgs/9-Brown.svg', '/assets/imgs/10-Grey.svg'
  ]
  public freeImagesList: any = ['/assets/imgs/1-free-Chair.svg', '/assets/imgs/2-free-Chair.svg', '/assets/imgs/3-free-Chair.svg',
    '/assets/imgs/4-free-Chair.svg', '/assets/imgs/5-free-Chair.svg', '/assets/imgs/6-free-Chair.svg', '/assets/imgs/7-free-Chair.svg',
    '/assets/imgs/8-free-Chair.svg', '/assets/imgs/9-free-Chair.svg', '/assets/imgs/10-free-Chair.svg']

  public bookedImagesList: any = ['/assets/imgs/1-booked-Chair.svg', '/assets/imgs/2-booked-Chair.svg', '/assets/imgs/3-booked-Chair.svg',
    '/assets/imgs/4-booked-Chair.svg', '/assets/imgs/5-booked-Chair.svg', '/assets/imgs/6-booked-Chair.svg', '/assets/imgs/7-booked-Chair.svg',
    '/assets/imgs/8-booked-Chair.svg', '/assets/imgs/9-booked-Chair.svg', '/assets/imgs/10-booked-Chair.svg']

  public serviceFee:number = 0;

  constructor( public translate: TranslateService,
    public service: SunspotdataService,
    public router: Router,
    public zone: NgZone,
    public route: ActivatedRoute) {

    if (this.route.snapshot.params['id']) {
      this.service.activeBeachId = this.route.snapshot.params['id'];
      if (this.service.beachesList.length) {
        this.service.getActiveBeachData();
      }

    }
  }

  ngOnInit(): void {
    var self = this;
    self.getSettingsData();
    var height = window.innerHeight
      || document.documentElement.clientHeight
      || document.body.clientHeight;

    height = height - 70;
    
    this.fullHeight = height + "px"; 
    this.seatFullHeight = height - 120 + "px"
   
    var element3 = document.getElementById('scollImageDiv3');
    if (element3) {
      element3.style.height = height + "px";
    }
    var element = document.getElementById('scollContentDiv2');
    if (element) {
      element.style.height = height + "px";
    }

  }

  checkScroll() {
   
    var childDiv = document.getElementById('rowDiv0') ;
    var mainDiv = document.getElementById('mainRowDiv0') ;

    if(mainDiv && childDiv && mainDiv.scrollWidth - 100 < childDiv.scrollWidth) {
      return true;
    } else {
      return false;
    }
  }

  moveScrollBar(i, check) {

    var contentDiv = document.getElementById('rowDiv'+i) ;
    const content_scroll_width = contentDiv.scrollWidth;
    let content_scoll_left = contentDiv.scrollLeft;
    if(check=='left') {
      content_scoll_left -= 100;
      if (content_scoll_left <= 0) {
          content_scoll_left = 0;
      }
      contentDiv.scrollLeft = content_scoll_left;
    } else {
      content_scoll_left += 100;
      if (content_scoll_left >= content_scroll_width) { content_scoll_left = content_scroll_width; }
      contentDiv.scrollLeft = content_scoll_left;
    
    }
   
  }

  changeVisisbility() {
    if (this.showSeats) {
      this.showSeats = false;
    } else {
      this.showSeats = true;
      this.updateSeatsBgImages();
    }
  }

  getSettingsData() {
    var self = this;
    self.loading = true;
    firebase.database().ref().child('settings/')
      .once('value', function (snapshot,) {
        self.settingsData = {};
        var data = snapshot.val();
        if (data) {
          self.settingsData = data;
          self.updateSeatsBgImages();
        }
        self.loading = false;
      }).catch( error => {
        self.loading = false;
        console.log(error);
      })
  }

  updateSeatsBgImages() {
    
    if (this.settingsData) {
     if (this.settingsData.seatsBgImage) {
       var div:any =  document.getElementById("scollImageDiv3") ;
       if(div) {
         div.style.background = "url(" + this.settingsData.seatsBgImage + ")  center center no-repeat";
       }
      }
    } 
  }

  checkBookedSeat(rowIndex, colIndex) {
    var index = this.service.selectedDateBookingsList.findIndex(x => x.row === rowIndex && x.col === colIndex);
    if (index >= 0) {
      return true;
    } else {
      return false;
    }
  }

  onSelectSeat(checked, rowIndex, colIndex) {

    if (!this.service.isLoggedIn) {
      return
    }
    var seatData = this.service.activeBeach.seatMaps.rows[rowIndex].col[colIndex]
    if (!checked) {
      this.service.activeBeach.seatMaps.rows[rowIndex].col[colIndex].booked = false;
      this.service.totalPrice = this.service.totalPrice - seatData.price;

      if (this.service.totalPrice < 1) {
        this.service.totalPrice = 0;
      }
      var index = this.service.bookedSeats.findIndex(x => x.row === rowIndex && x.col === colIndex);
      this.service.bookedSeats.splice(index, 1);
    } else {
      this.service.activeBeach.seatMaps.rows[rowIndex].col[colIndex].booked = true;
      this.service.totalPrice = this.service.totalPrice + seatData.price;

      var seat = {
        row: rowIndex,
        col: colIndex,
        price: seatData.price,
        // chair:seatData.chair, 
        text: seatData.text,
        color: seatData.color
      }
      this.service.bookedSeats.push(seat);
    }

  }

}
