import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  public language: any;
  public selectedWords: any;
  public enWords: any = {
    Actions: "Actions",
    Allow: "Allow",
    Back: "Back",
    Left: "Left",
    Delete: "Delete",
    Edit: "Edit",
    Save: "Save",
    Update: "Update",
    Submit: "Submit",
    Cancel: "Cancel",
    Camera: "Camera",
    Gallery: "Gallery",
    Logout: "Logout",
    Apply: "Apply",
    Next: "Next",
    Redeem: "Redeem",
    alRedeem: "Already Redeemed",
    choosePicMsg: "Choose picture from...",
    requiredField: "*This field is required",
    validEmailErr: "Enter a valid email",
    mktDelToast: "Market Deleted Successfully!",
    evtDelToast: "Event Deleted Successfully!",
    imgFileReq: "Image File is required.",
    endTimeErr: "End Time should be greater then Start Time",
    preDateErr: "Event date cannot be a prior date",
    noStoreRng: "You are not in stores proximity please be at store to redeem the coupons",
    resetPassMsg: "Reset password link sent via email",
    passSameErr: "Password and confirmed password are mismatched",
    acctCreateMsg: "Account Created Successfully",
    profUpdatedMsg: "Profile Updated Successfully",
    years: "years",
    year: "year",
    months: "months",
    month: "month",
    days: "days",
    day: "day",
    hours: "hours",
    hour: "hour",
    minutes: "minutes",
    minute: "minute",
    seconds: "seconds",
    second: "second",
    clothes:"Clothes",
    food:"Food",
    other:"Other",
    all:"All",
    viewUser:"View Reacted Users",
    allowAll:"Allow for all",
    wantNotify:"Do you want to get Notifications from the business?",
    followCoupon:"Follow (all icons/events marked as favourite for this business)",
    unfollowCoupon:"Unfollow (all icons/events marked as unfavourite for this business)",
    followBusiness:"Do you want to follow this business?",
    unfollowBusiness:"Do you want to unfollow this business?"
  }
  
  public frWords = {
    Actions: "Handlinger",
    Allow: "Tillate",
    Back: "Marked",
    Left: "Left",
    Delete: "Slette",
    Edit: "Redigere",
    Save: "Lagre",
    Update: "Oppdater",
    Submit: "Send Inn",
    Cancel: "Avbryt",
    Camera: "Kamera",
    Gallery: "Galleri",
    Logout: "Logg ut",
    Apply: "Apply",
    Next: "Neste",
    Redeem: "Redeem",
    alRedeem: "Allerede Innløst",
    choosePicMsg: "Velge et bilde fra...",
    requiredField: "Dette feltet er obligatorisk",
    validEmailErr: "Skriv inn et gyldig e-post adresse",
    mktDelToast: "Marked har blitt slettet!",
    evtDelToast: "Eventet har blitt slettet!",
    imgFileReq: "Bildefil er obligatorisk.",
    endTimeErr: "End Time should be greater then Start Time",
    preDateErr: "Event dato kan ikke være en forbigått dato",
    noStoreRng: "Du er ikke i nærheten av butikkene, vennligst gå til butikken for å aktivere kupongen",
    resetPassMsg: "Tilbakestilt passord er sendt til din e-post",
    passSameErr: "Angitt passord and bekreWet passord er ikke like.",
    acctCreateMsg: " Konto er opprettet",
    profUpdatedMsg: "Profiloppdatering er vellykket",
    years: "år",
    year: "år",
    months: "måneder",
    month: "måned",
    days: "dager",
    day: "dag",
    hours: "timer",
    hour: "time",
    minutes: "minutter",
    minute: "minutt",
    seconds: "sekunder",
    second: "sekund",
    clothes:"Klær",
    food:"Mat",
    other:"Annet",
    all:"Alt",
    viewUser:"Se Reagerte Brukere",
    allowAll:"Allow for alle",
    wantNotify:"Vil du få varsler fra virksomheten?",
    followCoupon:"Følg (alle ikoner / hendelser merket som favoritt for denne virksomheten)",
    unfollowCoupon:"Slutte å følge (alle ikoner / hendelser merket som ugunstige for denne virksomheten)",
    followBusiness:"Vil du følge denne virksomheten?",
    unfollowBusiness:"Vil du følge opp denne virksomheten?"
  }

  constructor(public translate: TranslateService) {

    if (!localStorage.getItem("SunspotLanguage")) {
      this.language = 'fr';
      translate.setDefaultLang('fr');
      translate.use('fr');
    } else {
      this.language = localStorage.getItem("SunspotLanguage");
      translate.setDefaultLang(this.language);
      translate.use(this.language);
    }

    if (this.language == 'en') {
      this.selectedWords = JSON.parse(JSON.stringify(this.enWords))
    } else {
      this.selectedWords = JSON.parse(JSON.stringify(this.frWords));
    }

  }


  public setLanguage(language) {
    this.language = language;
    if (this.language == 'en') {
      this.selectedWords = JSON.parse(JSON.stringify(this.enWords))
    } else {
      this.selectedWords = JSON.parse(JSON.stringify(this.frWords));
    }
    localStorage.setItem("SunspotLanguage", this.language);
    this.translate.setDefaultLang(language);
    this.translate.use(language);
  }

}
