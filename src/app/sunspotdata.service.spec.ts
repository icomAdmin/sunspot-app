import { TestBed } from '@angular/core/testing';

import { SunspotdataService } from './sunspotdata.service';

describe('SunspotdataService', () => {
  let service: SunspotdataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SunspotdataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
