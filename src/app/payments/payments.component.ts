import { Component, OnInit, NgZone } from '@angular/core';
import * as firebase from 'firebase';
import { SunspotdataService } from 'src/app/sunspotdata.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
declare var Stripe: any;

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss']
})
export class PaymentsComponent implements OnInit {

  public settingsData: any = {};
  public seatsArray: any = [];
  public submitted: boolean = false;
  public paymentForm: FormGroup;
  public cardNoError: boolean = false;
 // public paidOnSpot: boolean = false;
  public card = {
    name: "",
    number: "",
    cvc: "",
    exp_month: "1",
    exp_year: "2021"
  }

  public loading: boolean = false;
  public serviceFee: number = 0;

  public chairImagesList: any = ['/assets/imgs/1-White.svg', '/assets/imgs/2-Yellow.svg', '/assets/imgs/3-Orange.svg',
    '/assets/imgs/4-Pink.svg', '/assets/imgs/5-Purple.svg', '/assets/imgs/6-Blue.svg', '/assets/imgs/7-Cyan.svg',
    '/assets/imgs/8-Green.svg', '/assets/imgs/9-Brown.svg', '/assets/imgs/10-Grey.svg'
  ]


  constructor(private formBuilder: FormBuilder,
    public translate: TranslateService,
    public service: SunspotdataService,
    public router: Router,
    public zone: NgZone,
    private toastr: ToastrService,
    public route: ActivatedRoute) {

  }

  ngOnInit(): void {
    var self = this;
    if (!self.service.activeBeachId) {
      this.router.navigate(['/home']);
    }

    self.seatsArray = [];
    for (var i = 0, j = self.service.bookedSeats.length; i < j; i++) {
      var temp = JSON.parse(JSON.stringify(self.service.bookedSeats[i]))
      var idx = self.seatsArray.findIndex(obj => obj.text == temp.text && obj.price == temp.price);
      if (idx >= 0) {
        self.seatsArray[idx].seats.push({ row: temp.row, col: temp.col });
        self.seatsArray[idx].count++;
      } else {
        self.seatsArray.push({ text: temp.text, color: temp.color, price: temp.price, count: 1, seats: [{ row: temp.row, col: temp.col }] })
      }
    }
    self.getSettingsData();
    self.paymentForm = self.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(3)]],
      number: ['', [Validators.required, Validators.minLength(16), Validators.maxLength(16)]],
      cvv: ['', [Validators.required, Validators.min(100), Validators.max(9999)]],
      expMonth: ['', [Validators.required, Validators.min(1), Validators.max(12)]],
      expYear: ['', [Validators.required, Validators.min(2020), Validators.max(2040)]],
    });

    self.paymentForm.get("expMonth").setValue(1);
    self.paymentForm.get("expYear").setValue(2023);
    self.paymentForm.get("name").setValue(self.service.userData.firstName + ' ' + self.service.userData.lastName);

   // Stripe.setPublishableKey('pk_test_51HZePbELZK9H7puOKAeuAmdoohMpQOI2sNk9CUNYuzhv6goc3JsCjTqCprPmRScD7q1uWVNB60Bmq37rwC9Z8pv900N4v1qNtp');
    Stripe.setPublishableKey('pk_live_51IjPENLRKFtLZQcngM9fQig4JXV9Mo0wERyv4qVhkjo0yUDrvKKx4alOWLT1fOjLqIOMZX6Ngykld90rPL6s0n0O00YxslESVQ');

  }

  getSettingsData() {
    var self = this;
    self.loading = true;
    firebase.database().ref().child('settings/')
      .once('value', function (snapshot,) {
        self.settingsData = {};
        var data = snapshot.val();
        if (data) {
          self.settingsData = data;
          self.getServiceFee()
        }
        self.loading = false;
      }).catch(error => {
        self.loading = false;
        console.log(error);
      })
  }

  get f() { return this.paymentForm.controls; }

  formatNumber(event) {
    let value = event.target.value || '';
    value = value.replace(/[^0-9 ]/, '');
    event.target.value = value;
  }

  addCardPayment() {

    var self = this;
    self.submitted = true;
    if (self.paymentForm.valid && !self.cardNoError) {
      self.submitted = false;
      self.loading = true;
      self.getServiceFee();
      self.card.name = self.paymentForm.get("name").value;
      self.card.number = '' + self.paymentForm.get("number").value;
      self.card.cvc = self.paymentForm.get("cvv").value;
      self.card.exp_month = self.paymentForm.get("expMonth").value;
      self.card.exp_year = self.paymentForm.get("expYear").value;

      Stripe.card.createToken(this.card, (status, response) => {
        if (response.error) {
          self.loading = false;
          self.toastr.error(response.error.message, '', { timeOut: 4000 });
          return '';
        } else {
          var token = response.id;
          var params: any = {
            email: self.service.userData.email,
            token: token,
          }
          firebase.functions().httpsCallable("addCardToCustomer")(params)
            .then(res => {
              if (res.data.success) {
                this.chargeCustomer(res.data.data.customer_id);
              } else {
                self.zone.run(() => {
                  self.loading = false;
                  self.toastr.error(res.data.message.raw.message, '', { timeOut: 6000 });
                });
                return '';
              }
            }).catch(err => {
              self.zone.run(() => {
                self.loading = false;
                self.toastr.error(err, '', { timeOut: 4000 });
              });
              return '';
            })
        }
      });
    }
  }

  chargeCustomer(customerId) {
    var self = this;
    var price = 0, description: string = '';
    if ( this.service.activeBeach?.beachInfo?.paidOnSpot) {
      price = self.serviceFee;
      description = self.service.userData.firstName + ' ' + self.service.userData.lastName + ', booked ' + self.service.bookedSeats.length + ' seats. And paid only service fee ' + price + "€";
    } else {
      price = self.service.totalPrice + self.serviceFee;
      description = self.service.userData.firstName + ' ' + self.service.userData.lastName + ', booked ' + self.service.bookedSeats.length + ' seats. And paid total bill ' + self.service.totalPrice + "€";
    }
    firebase.functions().httpsCallable("chargeCustomer")({
      customerId: customerId,
      description: description,
      amount: price * 100,
    }).then(res => {
      if (res.data.success) {
        self.onSeatBooking(res.data.data, price);
      } else {
        self.loading = false;
        self.toastr.error(res.data.message.raw.message, '', { timeOut: 4000 });
        return '';
      }
    });
  }

  onSeatBooking(paymentRef: any, price) {
    var self = this;

    if (!self.service.activeBeach.totalEarning) {
      self.service.activeBeach.totalEarning = 0;
    } if (!self.service.activeBeach.currentBalance) {
      self.service.activeBeach.currentBalance = 0;
    }

    var totalEarning = self.service.activeBeach.totalEarning + price;
    var currentBalance = self.service.activeBeach.currentBalance + price;
    var formData = {
      uid: self.service.userData.uid,
      beachId: self.service.activeBeachId,
      bookingDate: moment(new Date(self.service.selectedDate)).format('YYYY-MM-DD') ,
      totalCharges: price,
      serviceFee: self.serviceFee,
      bookedSeats: self.service.bookedSeats,
      paymentRef: paymentRef,
      timeStamp: Number(new Date()),
      paidOnSpot: this.service.activeBeach?.beachInfo?.paidOnSpot,
    }

    var newBookingID = firebase.database().ref().child('userBookings/').push().key;

    var updates = {};
    updates['/userBookings/' + "/" + newBookingID] = formData;
    updates['/beaches/' + self.service.activeBeach.uid + "/totalEarning"] = totalEarning;
    updates['/beaches/' + self.service.activeBeach.uid + "/currentBalance"] = currentBalance;

    firebase.database().ref().update(updates).then(() => {
      self.service.activeBeach.totalEarning = self.service.activeBeach.totalEarning + price;
      self.service.activeBeach.currentBalance = self.service.activeBeach.currentBalance + price;

      formData['bookingId'] = newBookingID;
      self.service.userBookingsList.unshift(formData);

      if ( this.service.activeBeach?.beachInfo?.paidOnSpot) {
        self.toastr.success('You have booked ' + self.service.bookedSeats.length + ' seats, and paid service fees ' + price + '€ successfully.', '', { timeOut: 4000 });
      } else {
        self.toastr.success('You have booked ' + self.service.bookedSeats.length + ' seats, and paid total bill ' + price + '€ successfully.', '', { timeOut: 4000 });
      }

      self.service.getActiveBeachData();
      self.service.getActiveUserBookings();
      self.removeSelectedSeats();

      self.loading = false;
      self.router.navigate(['/place-detail', self.service.activeBeachId]);
    }).catch((err) => {
      self.service.activeBeach.totalEarning = self.service.activeBeach.totalEarning - price;
      self.service.activeBeach.currentBalance = self.service.activeBeach.currentBalance - price;

      self.toastr.error('Selected seats are not booked. Kindly try again', '', { timeOut: 4000 });
      self.loading = false;
      alert(err);
    });
  }

  removeSelectedSeats() {
    for (var i = 0; i < this.service.bookedSeats.length; i++) {
      this.service.activeBeach.seatMaps.rows[this.service.bookedSeats[i].row].col[this.service.bookedSeats[i].col].booked = false;
    }
    this.service.totalPrice = 0;
    this.service.bookedSeats = [];
  }

  invalidCardPattern() {
    var value = this.paymentForm.get('number').value;
    if (value && value.match(/^[0-9]+$/) === null) {
      this.cardNoError = true;
      return;
    }
    this.cardNoError = false
  }

  getServiceFee() {
    var self = this;
    self.serviceFee = 0;
    if (self.settingsData) {
      if (self.settingsData.selectedType != 'serviceFeePercent') {
        self.serviceFee = self.settingsData.serviceFeeAmount;
      } else {
        self.serviceFee = (self.service.totalPrice * self.settingsData.serviceFeePercent) / 100
      }
    }
    return self.serviceFee;
  }

}
