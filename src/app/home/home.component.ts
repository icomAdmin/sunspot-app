import { Component, OnInit, NgZone } from '@angular/core';
import { SunspotdataService } from 'src/app/sunspotdata.service';
import { Router } from '@angular/router';
import { AgmCoreModule } from '@agm/core';
import { TranslateService } from '@ngx-translate/core';
declare var google:any ;

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    public filters: any = {
        bar: false, restaurant: false, swimmingPool: false, shower: false, parking: false, wifi: false, wc: false
    }
    public moreFilters: boolean = false;
    public expandMap: boolean = false;

    public mapOptions:any = {
            latLngBounds: {
                north: 85,
                south: -85,
                west: -180,
                east: 180
            },
            strictBounds: true
        }
    
    public mapStyles = [
        {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#a0d6d1"
                },
                {
                    "lightness": 17
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#ffffff"
                },
                {
                    "lightness": 20
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#dedede"
                },
                {
                    "lightness": 17
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "color": "#dedede"
                },
                {
                    "lightness": 29
                },
                {
                    "weight": 0.2
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#dedede"
                },
                {
                    "lightness": 18
                }
            ]
        },
        {
            "featureType": "road.local",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#ffffff"
                },
                {
                    "lightness": 16
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#f1f1f1"
                },
                {
                    "lightness": 21
                }
            ]
        },
        {
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "color": "#ffffff"
                },
                {
                    "lightness": 16
                }
            ]
        },
        {
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "saturation": 36
                },
                {
                    "color": "#333333"
                },
                {
                    "lightness": 40
                }
            ]
        },
        {
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "transit",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#f2f2f2"
                },
                {
                    "lightness": 19
                }
            ]
        },
        {
            "featureType": "administrative",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#fefefe"
                },
                {
                    "lightness": 20
                }
            ]
        },
        {
            "featureType": "administrative",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "color": "#fefefe"
                },
                {
                    "lightness": 17
                },
                {
                    "weight": 1.2
                }
            ]
        }
    ]
    constructor(
        public translate: TranslateService,
        public service: SunspotdataService,
        public router: Router,
        public zone: NgZone) {
        if (this.service.isLoggedIn) {
            router.navigate(['']);
        } 
    }

    ngOnInit(): void {
        var height = window.innerHeight
        || document.documentElement.clientHeight
        || document.body.clientHeight;
    height = height - 70;
        if (this.service.fullBeachesList.length) {
            this.service.beachesList = this.service.fullBeachesList;
        }
        
        var element1:any = document.getElementById('scollContentDiv1');
        var element2:any = document.getElementById('scollContentDiv2');
        if (element1) {
            element1.style.maxHeight = height + "px";
        } if (element2) {
            element2.style.maxHeight = height + "px";
        }
    }

    getFilteredBeaches(value: any) {
        if (this.filters[value]) {
            this.filters[value] = false;
        } else {
            this.filters[value] = true;
        }

        this.service.beachesList = [];

        for (var i = 0; i < this.service.fullBeachesList.length; i++) {

            if (this.service.fullBeachesList[i].beachInfo && (!this.filters.bar || (this.filters.bar && this.service.fullBeachesList[i].beachInfo.bar)) &&
                (!this.filters.restaurant || (this.filters.restaurant && this.service.fullBeachesList[i].beachInfo.restaurant)) &&
                (!this.filters.swimmingPool || (this.filters.swimmingPool && this.service.fullBeachesList[i].beachInfo.swimmingPool)) &&
                (!this.filters.shower || (this.filters.shower && this.service.fullBeachesList[i].beachInfo.shower)) &&
                (!this.filters.parking || (this.filters.parking && this.service.fullBeachesList[i].beachInfo.parking)) &&
                (!this.filters.wifi || (this.filters.wifi && this.service.fullBeachesList[i].beachInfo.wifi)) &&
                (!this.filters.wc || (this.filters.wc && this.service.fullBeachesList[i].beachInfo.wc))) {

                this.service.beachesList.push(this.service.fullBeachesList[i]);

            } else if (!this.service.fullBeachesList[i].beachInfo && !this.filters.bar && !this.filters.restaurant && !this.filters.swimmingPool
                && !this.filters.shower && !this.filters.parking && !this.filters.wifi && !this.filters.wc) {

                this.service.beachesList.push(this.service.fullBeachesList[i]);

            }
        }

    }

    toggleMoreFilters() {
        if (!this.moreFilters) {
            this.moreFilters = true;
        } else {
            this.moreFilters = false;
            this.filters.wifi = false;
            this.filters.wc = true;
            this.getFilteredBeaches('wc');
        }
    }

    visitBeach(beachId) {
        this.router.navigate(['/place-detail', beachId]);
    }


}
